import Debug from 'debug';
import RssFeedSelector from '../rss-feed-data/feed-selector/FeedSelector.vue'
import RssFeedFrequentWords from '../rss-feed-data/frequent-words/FrequentWords.vue'
import RssFeedEntriesTable from '../rss-feed-data/entries-table/EntriesTable.vue'

const debug = Debug('component:Home');

export default {
    data() {
        return {
            feedId: -1,
            feedData: null,
            feedList: null,
            error: null
        }
    },
    components: {
        'rss-feed-selector': RssFeedSelector,
        'rss-feed-frequent-words': RssFeedFrequentWords,
        'rss-feed-entries-table': RssFeedEntriesTable,
    },
    methods: {
        loadFeedList() {
            this.error = null;
            this.apiRequest('GET', '/api/get-rss-feed-list', []).then( res => {
                if (res.success) {
                    this.feedList = res.data;
                    if (this.feedList.length > 0 ) {
                        this.feedId = this.feedList[0].id;
                        this.loadFeed(this.feedId);
                    }
                } else {
                    console.warn('Failed to load feed list');
                }
                this.$forceUpdate();
            });
        },
        loadFeed(feedId) {
            this.error = null;
            this.feedId = feedId;
            this.apiRequest('GET', `/api/get-rss-feed-results/${this.feedId}`, []).then( res => {
                if (res.success) {
                    this.feedData = res.data;
                } else {
                    this.feedData = null;
                    console.warn('Failed to load feed data');
                }
                this.$forceUpdate();
            });
        },
        getFeedWordItems() {
            if (this.feedData) {
                return this.feedData.feed_words;
            }
            return null;
        },
        getFeedEntries() {
            if (this.feedData) {
                return this.feedData.feed_entries;
            }
            return null;
        },
        getFeedList() {
            return this.feedList;
        },
        getSelectedFeed() {
            return this.feedData;
        },
    },
    mounted() {
        debug('mounted.');
        this.loadFeedList();
    },
};
