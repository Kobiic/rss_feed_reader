import Debug from 'debug';

const debug = Debug('component:Register');

export default {
    data() {
        return {
            email: '',
            password: '',
            password_confirmation: '',
            errors: [],
        }
    },
    methods: {
        getError(type) {
            if (this.errors[type]) {
                return this.errors[type].toString();
            }
            return null;
        },
        onRegister(e) {
            e.preventDefault();
            const data = {
                email: this.email,
                password: this.password,
                password_confirmation: this.password_confirmation
            };
            this.password = '';
            this.password_confirmation = '';
            this.apiRequest('POST', '/api/register', data).then( res => {
                if (res.success) {
                    this.errors = [];
                    this.$session.set('auth-token', res.success.token);
                    this.$router.push('/home');
                } else if (res.error) {
                    this.errors = res.error;
                }
            });
        },
        validateEmail() {
            const data = {
                email: this.email,
            };
            this.apiRequest('POST', '/api/check-email', data).then( res => {
                if (res.success) {
                    this.errors.email = null;
                } else if (res.error) {
                    this.errors = res.error;
                }
                this.$forceUpdate();
            });
        },
    },
    mounted() {
        debug('mounted.');
    },
};
