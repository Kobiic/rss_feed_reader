import Debug from 'debug';

const debug = Debug('component:Login');

export default {
    data() {
        return {
            email: '',
            password: '',
            errors: [],
        }
    },
    methods: {
        getError(type) {
            return this.errors[type];
        },
        onLogin(e) {
            e.preventDefault();
            const data = {
                email: this.email,
                password: this.password
            };
            this.password = '';
            this.apiRequest('POST', '/api/login', data).then( res => {
                if (res.success) {
                    this.errors = [];
                    this.$session.set('auth-token', res.success.token);
                    this.$router.push('/home');
                } else if (res.error) {
                    this.errors.login = res.error;
                }
                this.$forceUpdate();
            });
        },
    },
    mounted() {
        debug('mounted.');
    },
};
