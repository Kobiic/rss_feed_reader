import Debug from 'debug';

const debug = Debug('component:RssFeedData->EntriesTable');

export default {
    props: ['feedEntries'],
    computed: {
        feedEntriesfields() {
            return [
                {key: 'title', sortable: true, hrefKey: 'link'},
                {key: 'author_name', label: 'Author', sortable: true, hrefKey: 'author_link'},
                {key: 'summary', sortable: true},
            ];
        },
    },
    methods: {
        getFeedEntries() {
            return this.feedEntries;
        },
    },
    mounted() {
        debug('mounted.');
    },
};
