import Debug from 'debug';

const debug = Debug('component:RssFeedData->FeedSeletor');

export default {
    props: ['feedList', 'selectedFeed'],
    data() {
        return {
            selectedFeedId: null,
            disableActions: false
        };
    },
    watch: {
        selectedFeedId: function(val) {
            this.selectedFeedId = val;
            if (this.selectedFeed && this.selectedFeed.id != val) {
                this.$emit('change-feed', val);
            }
        },
    },
    methods: {
        getFeedOptions() {
            if (this.selectedFeed && !this.selectedFeedId) {
                this.selectedFeedId = this.selectedFeed.id;
            }
            const options = [];
                if (this.feedList) {
                this.feedList.forEach(function(item) {
                    options.push({ value: item.id, text: item.feed_link });
                });
            }
            return options;
        },
        getSelectedFeedErrors() {
            if (this.selectedFeed && this.selectedFeed.load_errors) {
                const text = this.selectedFeed.load_errors.join('; ');
                if (text) {
                    return text;
                }
            }
            return null;
        },
        updateFeed() {
            this.disableActions = true;
            this.apiRequest('GET', `/api/load-rss-feed/${this.selectedFeedId}`, []).then( res => {
                this.$emit('change-feed', this.selectedFeedId);
                this.disableActions = false;
            });
        },
    },
    mounted() {
        debug('mounted.');
    },
};
