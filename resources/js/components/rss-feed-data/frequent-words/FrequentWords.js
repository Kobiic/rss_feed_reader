import Debug from 'debug';

const debug = Debug('component:RssFeedData->FrequentWords');

export default {
    props: ['feedWords'],
    computed: {
        feedWordsfields() {
            return [
                {key: 'word', sortable: true},
                {key: 'times', sortable: true}
            ];
        },
    },
    methods: {
        getFeedWordItems() {
            return this.feedWords;
        },
    },
    mounted() {
        debug('mounted.');
    },
};
