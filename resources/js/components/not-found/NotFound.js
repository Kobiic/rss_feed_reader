import Debug from 'debug';

const debug = Debug('component:NotFound');

export default {
  mounted() {
    debug('mounted.');
  },
};
