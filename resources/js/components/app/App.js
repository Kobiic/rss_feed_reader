import Debug from 'debug';

const debug = Debug('component:App');

export default {
  methods: {
    onLogout() {
        this.apiRequest('POST', '/api/logout', []).then( res => {
            this.$session.clear();
            this.$router.push('/user/login');
        });
    },
    isLoggedIn() {
        return this.$session.exists('auth-token');
    },
    checkSession(firstLoad = false) {
        if (this.isLoggedIn()) {
            this.apiRequest('GET', '/api/user/details', []).then( res => {
                if (!res.success) {
                    this.onLogout();
                }
            });
        }
    },
  },
  mounted() {
    debug('mounted.');
    setInterval(function(){this.checkSession()}.bind(this), 10000);
  },
};
