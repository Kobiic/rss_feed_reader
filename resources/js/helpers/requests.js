export default {
    methods: {
        apiRequest(method, path, data) {
            const headers = new Headers();
            headers.set('Content-Type', 'application/json');
            headers.set('Accept', 'application/json');
            const apiKey = this.$session.get('auth-token');
            if (apiKey) {
              headers.set('Authorization', `Bearer ${apiKey}`);
            }
            return fetch( path, {
                method,
                headers,
                body: (method === 'GET' || method === 'HEAD') ? undefined : JSON.stringify(data),}
            ).then(res => {
                return res.json();
            });
        },
    },
};
