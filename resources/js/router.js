import Vue from 'vue';
import VueRouter from 'vue-router';
import Debug from 'debug';
import VueSessionStorage from 'vue-sessionstorage'

// User actions
import NotFound from './components/not-found/NotFound.vue'
import UserLogin from './components/user/login/Login.vue';
import UserRegister from './components/user/register/Register.vue';
import Home from './components/home/Home.vue';

const debug = Debug('router');
Vue.use(VueSessionStorage);
Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: '/', redirect: '/user/login' },
        { path: '/login', redirect: '/user/login' },
        //USER
        { path: '/user/login', name: 'user-login', component: UserLogin },
        { path: '/user/register', name: 'user-register', component: UserRegister },

        // LOGGED IN
        { path: '/home', name: 'home', component: Home },
        //DEFAULT
        { path: '*', name: 'not-found', component: NotFound },
    ]
});

const unauthorizedRoutes = ['user-login', 'user-register', 'not-found'];
const commonRoutes = ['not-found'];

router.beforeEach((to, from, next) => {
    // Redirect to home if logged and opening page accessable when logged out
    if (commonRoutes.indexOf(to.name) < 0 && Vue.prototype.$session.exists('auth-token') && unauthorizedRoutes.indexOf(to.name) > -1) {
        return next('/home');
    } // Redirect to login if not logged
    else if (unauthorizedRoutes.indexOf(to.name) < 0 && !Vue.prototype.$session.exists('auth-token')) {
      return next('/user/login');
    }
    return next();
});

export default router;
