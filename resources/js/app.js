import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue';
import VueRouter from 'vue-router';
import VueSessionStorage from 'vue-sessionstorage'

import Router from './router';
import App from './components/app/App.vue'

import HelperRequests from './helpers/Requests';

Vue.use(BootstrapVue);
Vue.use(VueSessionStorage);
Vue.use(VueRouter);

Vue.mixin(HelperRequests);

const app = new Vue({
    el: '#app',
    router: Router,
    template: '<app/>',
    components: {
      app: App,
    },
});
