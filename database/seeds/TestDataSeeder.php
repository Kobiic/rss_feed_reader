<?php

use Illuminate\Database\Seeder;
use App\RssFeeds;
use App\User;
use App\Http\Controllers\RssFeedController;
class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['email' => 'test@test.com'], ['name' => 'test@test.com', 'password' => bcrypt('test')]);
        User::updateOrCreate(['email' => 'test2@test.com'], ['name' => 'test2@test.com', 'password' => bcrypt('test')]);

        $userId = User::first()->id;
        RssFeeds::updateOrCreate(['feed_link' => "https://www.theregister.co.uk/devops/headlines.atom"], ['is_default' => 0, 'user_id' => $userId]);
        RssFeeds::updateOrCreate(['feed_link' => "https://www.theregister.co.uk/business/headlines.atom"]);


        \App::call('App\Http\Controllers\RssFeedController@loadMissingFeeds');
    }
}
