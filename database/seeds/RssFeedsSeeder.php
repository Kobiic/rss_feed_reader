<?php

use Illuminate\Database\Seeder;
use App\RssFeeds;

class RssFeedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RssFeeds::updateOrCreate(['feed_link' => "https://www.theregister.co.uk/software/headlines.atom"]);
    }
}
