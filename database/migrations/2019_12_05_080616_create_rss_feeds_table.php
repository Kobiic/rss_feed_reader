<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_feeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->string('feed_link', 2000)->nullable();
            $table->string('title')->nullable();
            $table->string('article_link', 2000)->nullable();
            $table->string('author_name')->nullable();
            $table->string('author_link', 2000)->nullable();
            $table->string('author_email')->nullable();
            $table->text('subtitle')->nullable();
            $table->boolean('is_default')->default(true);
            $table->tinyInteger('load_status')->default(0);
            $table->json('load_errors')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_feeds');
    }
}
