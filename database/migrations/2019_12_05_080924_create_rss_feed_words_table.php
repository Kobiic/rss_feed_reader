<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssFeedWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_feed_words', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rss_feed_id');
            $table->string('word');
            $table->integer('times')->default(0);

            $table->foreign('rss_feed_id')->references('id')->on('rss_feeds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_feed_words');
    }
}
