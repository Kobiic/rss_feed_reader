<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssFeedEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_feed_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rss_feed_id');

            $table->string('title')->nullable();
            $table->string('link')->nullable();
            $table->string('author_name')->nullable();
            $table->string('author_link')->nullable();
            $table->string('author_email')->nullable();
            $table->text('summary')->nullable();

            $table->foreign('rss_feed_id')->references('id')->on('rss_feeds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_feed_entries');
    }
}
