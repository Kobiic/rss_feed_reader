USES:
 - php 7.3.12
 - composer 1.9.1
 - yarn 1.3.2
 - npm 5.6.0

 
 
HOW TO SETUP:
1) Check ".env" file:
    1.1) Set APP_URL
    1.2) Modify DB_* to match empty schema

2) Via terminal:
	2.1) npm install
    2.2) composer install
    2.3) php artisan migrate
    2.4) php artisan db:seed
	2.5) php artisan passport:install
    2.6) php artisan serve

3) In second terminal:
    3.1)yarn hot
	
	

INFO:
	1) 2 users are created by default : `test@test.com`, `test2@test.com`
	2) DB table `rss_feeds` - can be used to add extra links
	
