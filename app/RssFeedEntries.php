<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RssFeeds;

class RssFeedEntries extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'rss_feed_id',
        'title', 'link',
        'author_name', 'author_link', 'author_email',
        'summary'
    ];

    public function rssFeed() {
        return $this->belongsTo(RssFeeds::class, 'rss_feed_id');
    }
}
