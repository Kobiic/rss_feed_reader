<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RssFeeds;


class RssFeedWords extends Model
{
    public $timestamps = false;
    protected $fillable = ['rss_feed_id', 'word', 'times'];

    public function rssFeed() {
        return $this->belongsTo(RssFeeds::class, 'rss_feed_id');
    }
}
