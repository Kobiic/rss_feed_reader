<?php

function Helper_CurlFile($url)
{
    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $url);
    curl_setopt($resURL, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($resURL, CURLOPT_FAILONERROR, true);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, false);

    $response = curl_exec($resURL);
    curl_close($resURL);

    return $response;
}

/**
 * Get SimpleXml first key value.
 */
function Helper_XmlGetValue($xml, $param)
{
    if (isset($xml->{$param})) {
        if (!is_array($xml->{$param})) {
            return $xml->{$param};
        } else {
            return $xml->{$param}[0];
        }
    }
    return null;
}

/**
 * Get SimpleXml first href (ignores self links)
 */
function Helper_XmlGetHref($xml, $param = 'link')
{
    if (isset($xml->{$param})) {
        if (!is_array($xml->{$param})) {
            foreach ($xml->{$param} as $linkEl) {
                $res = Helper_getUsableLink($linkEl);
                if ($res != null) {
                    return $res;
                }
            }
        } else {
            return Helper_getUsableLink($xml->{$param});
        }
    }
    return null;
}

function Helper_getUsableLink($xmlEl) {
    if (!empty($xmlEl->attributes()) && isset($xmlEl->attributes()->href) &&
    (!isset($xmlEl->attributes()->rel) || $xmlEl->attributes()->rel !=  'self')
    ) {
        return $xmlEl->attributes()->href;
    }
    return null;
}
