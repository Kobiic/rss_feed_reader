<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Common words (single language - english).
class CommonWords extends Model
{
    // Order determines how common word is to be able to escape top X words
    protected $fillable = ['word', 'order'];
}
