<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\RssFeedEntries;
use App\RssFeedWords;

class RssFeeds extends Model
{
    const LOAD_STATUS_CLEAR = 0;
    const LOAD_STATUS_DONE = 1;
    const LOAD_STATUS_LOADING = 2;

    protected $fillable = [
        'rss_feed_id',
        'feed_link', 'title', 'article_link',
        'author_name', 'author_link', 'author_email',
        'subtitle',
        'is_default', 'load_status'
    ];

    protected $casts = [
        'load_errors' => 'array'
    ];

    public function getRouteKeyName() {
        return 'id';
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function feedEntries() {
        return $this->hasMany(RssFeedEntries::class, 'rss_feed_id');
    }

    public function feedWords() {
        return $this->hasMany(RssFeedWords::class, 'rss_feed_id');
    }
}
