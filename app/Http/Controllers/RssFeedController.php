<?php

namespace App\Http\Controllers;

use App\CommonWords;
use App\RssFeeds;
use App\RssFeedEntries;
use App\RssFeedWords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RssFeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RssFeeds  $rssFeed
     * @return \Illuminate\Http\Response
     */
    public function show(RssFeeds $rssFeed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RssFeeds  $rssFeed
     * @return \Illuminate\Http\Response
     */
    public function edit(RssFeeds $rssFeed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RssFeeds  $rssFeed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RssFeeds $rssFeed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RssFeeds  $rssFeed
     * @return \Illuminate\Http\Response
     */
    public function destroy(RssFeeds $rssFeed)
    {
        //
    }

    // Can be used to update all missing feeds
    public function loadMissingFeeds()
    {
        $feeds = RssFeeds::where('load_status', RssFeeds::LOAD_STATUS_CLEAR)->get();
        $res = ['success' => [], 'error' => []];
        foreach ($feeds as $feed) {
            $loadRes = (array) $this->loadRssFeed($feed)->getData();
            if (isset($loadRes['success'])) {
                $res['success'][] = $feed->feed_link;
            } else {
                $res['errors'][] = ['link' => $feed->feed_link, 'error' => $loadRes['error']];
            }
        }
        return response()->json($res);
    }

    public function loadRssFeed(RssFeeds $rssFeed)
    {
        // Implement safe updating - don't update if already loading....
        \DB::beginTransaction();
        $rssFeed = RssFeeds::where('id', $rssFeed->id)->first();
        if (!$rssFeed || $rssFeed->load_status == RssFeeds::LOAD_STATUS_LOADING) {
            \DB::rollBack();
            return response()->json(['error' => 'Feed already updating'], 401);
        }
        $rssFeed->load_status = RssFeeds::LOAD_STATUS_LOADING;
        $rssFeed->save();
        \DB::commit();

        $errors = [];
        $response = false;
        if (filter_var($rssFeed->feed_link, FILTER_VALIDATE_URL) === FALSE) {
            $errors[] = 'Invalid RSS feed link';
        } else {
            $response = Helper_CurlFile($rssFeed->feed_link);
        }

        if ($response !== false) {
            $xml = simplexml_load_string($response);
            if ($xml) {
                // Create and save base & entries data of feed
                $rssFeed->feedEntries()->delete();

                $rssFeed->title = Helper_XmlGetValue($xml, 'title');
                $rssFeed->article_link = Helper_XmlGetHref($xml);
                $rssFeed->subtitle = Helper_XmlGetValue($xml, 'subtitle');

                if(!empty($xml->author)) {
                    $rssFeed->author_name = Helper_XmlGetValue($xml->author, 'name');
                    $rssFeed->author_link = Helper_XmlGetValue($xml->author, 'uri');
                    $rssFeed->author_email = Helper_XmlGetValue($xml->author, 'email');
                } else {
                    $rssFeed->author_name = null;
                    $rssFeed->author_link = null;
                    $rssFeed->author_email = null;
                }
                $rssFeed->save();

                if (!empty($xml->entry)) {
                    foreach ($xml->entry as $entry) {
                        $entryData = [
                            'rss_feed_id' => $rssFeed->id,
                            'author_name' => null,
                            'author_link' => null,
                            'author_email' => null
                        ];
                        $entryData['title'] = Helper_XmlGetValue($entry, 'title');
                        $entryData['link'] = Helper_XmlGetHref($entry);
                        $entryData['summary'] = strip_tags(html_entity_decode(Helper_XmlGetValue($entry, 'summary')));
                        if(!empty($entry->author)) {
                            $entryData['author_name'] = Helper_XmlGetValue($entry->author, 'name');
                            $entryData['author_link'] = Helper_XmlGetValue($entry->author, 'uri');
                            $entryData['author_email'] = Helper_XmlGetValue($entry->author, 'email');
                        }
                        RssFeedEntries::create($entryData);
                    }
                }

                // Count unique words
                $rssFeed->feedWords()->delete();

                // escape html tags in xml and xml element text
                $textData = strtolower(strip_tags(html_entity_decode($xml->asXML())));
                // remove common used sentence separators. links still be detected
                $textData = preg_replace('/[.,!\?…+]+/', '', $textData);
                $textData = explode(' ', $textData);
                $textData = array_count_values($textData);

                $wordCountData = [];
                foreach ($textData as $word => $recCount) {
                    $baseWord = $word;
                    // is word if contains only letters and -. Can have single aposthrope or slash. Can have 1 extra charater at start/end, that will be removed
                    if (preg_match('/^[^a-z]?[a-z\-]+[\'\/]?[a-z\-]+[^a-z]?$/', $word)) {
                        // remove extra character that is not letter at start/finish (qoutes and other symbols like that)
                        $word = preg_replace('/(^[^a-z]?)|([^a-z]?$)/', '', $word);
                        if (isset($wordCountData[$word])) {
                            $wordCountData[$word] = $recCount;
                        } else {
                            $wordCountData[$word] = $recCount;
                        }
                    }
                }

                foreach ($wordCountData as $word => $recCount) {
                    RssFeedWords::create([
                        'rss_feed_id' => $rssFeed->id,
                        'word' => $word,
                        'times' => $recCount
                    ]);
                }
            } else {
                $errors[] = 'Invalid XML data';
            }
        } else if(empty($errors)) {
            $errors[] = 'Failed to load feed';
        }

        $rssFeed->load_errors = $errors;
        if (empty($errors)) {
            $rssFeed->load_status = RssFeeds::LOAD_STATUS_DONE;
        } else {
            $rssFeed->load_status = RssFeeds::LOAD_STATUS_CLEAR;
        }
        $rssFeed->save();

        if (!empty($errors)) {
            return response()->json(['error' => $rssFeed->load_errors], 401);
        }
        return response()->json(['success' => 'Feed loaded']);
    }

    public function getRssFeedList()
    {
        $result = RssFeeds::select('id', 'title', 'feed_link')
            ->where('is_default', 1)
            ->orWhere('user_id', Auth::user()->id)
            ->get();
        return response()->json(['success' => true, 'data' => $result]);
    }

    public function getRssFeedResults(RssFeeds $rssFeed, $wordLimitCount = 10, $commonWordCount = 50)
    {
        if (!$rssFeed->is_default && $rssFeed->user_id != Auth::user()->id) {
            return response()->json(['error' => 'Access denied'], 401);
        }

        $result = $rssFeed->toArray();
        $result['feed_entries'] = $rssFeed->feedEntries->toArray();

        // Select top {$wordLimitCount} words excluding {$commonWordCount} amount fo words
        $result['feed_words'] = $rssFeed->feedWords()
            ->whereNotIn('word', CommonWords::where('order', '<=', $commonWordCount)->pluck('word'))
            ->orderBy('times', 'DESC')
            ->orderBy('word', 'ASC')
            ->limit($wordLimitCount)
            ->get()
            ->toArray();

        return response()->json(['success' => true, 'data' => $result]);
    }
}
