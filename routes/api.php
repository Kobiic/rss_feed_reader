<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',  [ 'as' => 'login', 'uses' => 'UserController@login']);
Route::post('logout', 'UserController@logout');
Route::post('check-email', 'UserController@checkEmail');
Route::post('register', 'UserController@register');

Route::get('load-new-feeds', 'RssFeedController@loadMissingFeeds');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user/details', 'UserController@getDetails');
    Route::get('load-rss-feed/{rssfeed}', 'RssFeedController@loadRssFeed');
    Route::get('get-rss-feed-list', 'RssFeedController@getRssFeedList');
    Route::get('get-rss-feed-results/{rssfeed}/{wordLimitCount?}/{commonWordCount?}', 'RssFeedController@getRssFeedResults')
         ->where('wordLimitCount', '[0-9]+')->where('commonWordCount', '[0-9]+');
});
